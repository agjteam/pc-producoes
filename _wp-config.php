<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'onetonedb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '14789632');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+<BsRq!%g$|~/5op((5-|*e06Zjb2m6`M,`]8/pi4|Ezi0P8F^p2KgoEi8XN.ZC3');
define('SECURE_AUTH_KEY',  '_iw]VN>:]@(/8Fy$?Y.<KhOb:B.|-%Ukhve)mAnJ:]1cN2jTdCoKGqA54M.V,esU');
define('LOGGED_IN_KEY',    'AZ{3|/tA$9_{uFmYjPGZh:0&(*k`d=<Q#pma6%r*pXQd+%j+}DlqJi./)2TmDd1^');
define('NONCE_KEY',        'p=$MTc}r)GFdCex~uKurCPced?JAKre6}Cz@Kn}T 8BU]2wK{S4Pp:fa_LSEA>VP');
define('AUTH_SALT',        '3<-Iz8S8Cd31160$,G&u[HAOu+_*J7kOvt?[O _luXl,nz+5L{GJ]2C5,Wz:Mr$!');
define('SECURE_AUTH_SALT', '_K@g_7=qq;uNyqC6B_$wCtFVbu}zTwZ&4MT;Pm=c2jur--VJ2ofCuk> MK2WBmHU');
define('LOGGED_IN_SALT',   '(^;uJ6ppGtkA;#Vn}]&I.#T._ntqo4s<)n{,A6(SgFU:M_MLPcI_tZ}*/@R?K>)Z');
define('NONCE_SALT',       'P3t)(2)PkNQYFvs/m?|;CSwp^Ej]dCTO$>U%2~sLgYJJgX6ZDTd{L)6d<zsv_l7Q');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
